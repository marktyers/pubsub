
var redis = require("ioredis");
var publisher  = redis.createClient();

// publishes the current date and time to the 'test' channel every minute
setInterval(function() {
    var date = new Date();
    console.log(date);
    publisher.publish('test', date);
}, 10 * 1000);
