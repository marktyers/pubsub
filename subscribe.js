var redis = require("ioredis");
var subscriber  = redis.createClient();

subscriber.subscribe('test');

subscriber.on('message', function(channel, message) {
  console.log('Message ' + message + ' on channel ' + channel + ' arrived!');
});
